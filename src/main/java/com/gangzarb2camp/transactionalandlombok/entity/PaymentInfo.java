package com.gangzarb2camp.transactionalandlombok.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "PAYMENT_INFO")
@Builder
public class PaymentInfo {
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
    private String paymentId;

    @Column(name = "nominal", nullable = false)
    private int nominal;

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private Customer customer;

}
