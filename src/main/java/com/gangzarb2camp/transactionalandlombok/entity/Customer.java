package com.gangzarb2camp.transactionalandlombok.entity;


import lombok.*;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Getter
@Setter
@Table(name = "m_cust")
public class Customer {

    @Id
    @Column(name = "id",nullable = false,length = 50)
    private String id;

    @Column(name = "nama",nullable = false,length = 100)
    private String nama;

    @Column(name = "nominal",nullable = false)
    private int nominal;

    @Transient
    private String alamat;

}
