package com.gangzarb2camp.transactionalandlombok.model;

import com.gangzarb2camp.transactionalandlombok.entity.Customer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaymentBookingAcknowledgment {

    private String status;
    private double totalPayment;
    private String kwitansiNo;
    private Customer customerInfo;
}
