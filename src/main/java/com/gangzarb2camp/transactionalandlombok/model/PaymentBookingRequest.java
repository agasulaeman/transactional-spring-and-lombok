package com.gangzarb2camp.transactionalandlombok.model;

import com.gangzarb2camp.transactionalandlombok.entity.Customer;
import com.gangzarb2camp.transactionalandlombok.entity.PaymentInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaymentBookingRequest {

    private Customer customerInfo;

    private PaymentInfo paymentInfo;
}
