package com.gangzarb2camp.transactionalandlombok.utils;

import com.gangzarb2camp.transactionalandlombok.exception.InsufficientAmmountException;

import java.util.HashMap;
import java.util.Map;

public class PaymentUtils {

    private static Map<String, Double> paymentMap = new HashMap<>();

    {
        paymentMap.put("agas1", 10000.0);
        paymentMap.put("agas2", 20000.0);
        paymentMap.put("agas3", 30000.0);
        paymentMap.put("agas4", 40000.0);
        paymentMap.put("agas5", 50000.0);
    }

    public static boolean validateCreditLimit(String accountNo, double paidAmmount) {
        if (paidAmmount > paymentMap.get(accountNo)) {
            throw new InsufficientAmmountException("Kurang bayar, Harap melunasi terlebih dahulu");
        } else {
            return true;
        }
    }
}
