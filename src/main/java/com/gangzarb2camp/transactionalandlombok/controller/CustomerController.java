package com.gangzarb2camp.transactionalandlombok.controller;

import com.gangzarb2camp.transactionalandlombok.entity.Customer;
import com.gangzarb2camp.transactionalandlombok.service.CustomerService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/customer")
@RestController
@AllArgsConstructor
public class CustomerController {

    private CustomerService customerService;

    @GetMapping
    public Iterable<Customer> getAll() {
        return customerService.findAll();
    }

    @PostMapping
    public String save(@RequestBody Customer customer) {
        return customerService.create(customer);
    }

    @PutMapping("{id}/{nominal}")
    public Customer update(@PathVariable("id") String id, @PathVariable("nominal") int nominal) {
        return customerService.update(id, nominal);
    }
}

