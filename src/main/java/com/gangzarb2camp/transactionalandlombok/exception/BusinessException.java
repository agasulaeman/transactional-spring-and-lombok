package com.gangzarb2camp.transactionalandlombok.exception;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class BusinessException extends RuntimeException{

        private String code;
        private String message;
}
