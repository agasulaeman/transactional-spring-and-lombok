package com.gangzarb2camp.transactionalandlombok.exception;

public class InsufficientAmmountException extends  RuntimeException{

    public InsufficientAmmountException(String message) {
        super(message);
    }
}
