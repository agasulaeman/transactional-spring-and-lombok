package com.gangzarb2camp.transactionalandlombok.repository;

import com.gangzarb2camp.transactionalandlombok.entity.Customer;
import org.springframework.data.repository.CrudRepository;

public interface CustomerRepository extends CrudRepository<Customer,String> {

}
