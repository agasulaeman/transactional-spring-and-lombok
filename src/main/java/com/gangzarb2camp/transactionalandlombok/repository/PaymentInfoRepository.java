package com.gangzarb2camp.transactionalandlombok.repository;

import com.gangzarb2camp.transactionalandlombok.entity.PaymentInfo;
import org.springframework.data.repository.CrudRepository;

public interface PaymentInfoRepository extends CrudRepository<PaymentInfo,String> {

}
