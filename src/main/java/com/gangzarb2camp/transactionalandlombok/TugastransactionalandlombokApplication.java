package com.gangzarb2camp.transactionalandlombok;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class TugastransactionalandlombokApplication {


    public static void main(String[] args) {
        SpringApplication.run(TugastransactionalandlombokApplication.class, args);
    }

}
