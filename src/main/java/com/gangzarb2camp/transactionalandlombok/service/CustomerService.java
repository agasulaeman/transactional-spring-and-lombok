package com.gangzarb2camp.transactionalandlombok.service;

import com.gangzarb2camp.transactionalandlombok.entity.Customer;
import com.gangzarb2camp.transactionalandlombok.entity.PaymentInfo;
import com.gangzarb2camp.transactionalandlombok.repository.CustomerRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
@Slf4j
public class CustomerService {


    private CustomerRepository customerRepository;


    private PaymentInfoService paymentInfoService;


    @Transactional //BEGIN //isolation read commited dan propagation required
    public String create(Customer customer) {
        Customer custSave = customerRepository.save(customer);  //insert into
        log.info("user save {} {}", custSave, "success");
        PaymentInfo paymentInfo = paymentInfoService.save(custSave, custSave.getNominal());
        log.info("saving payment info account success {}", paymentInfo);
        return "success"; //commit
    }

    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    public Iterable<Customer> findAll() {
        var custSave = customerRepository.findAll();
        log.info("user save {} ", custSave);
        return custSave;
    }

    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public Customer update(String id, int nominal) {
        var custFind = customerRepository.findById(id)
                .stream()
                .peek(customer -> customer.setNominal(customer.getNominal() + nominal))
                .findFirst().orElseThrow();
        Customer custSave = customerRepository.save(custFind);
        System.out.println(custSave);
        return custSave;
    }

}
