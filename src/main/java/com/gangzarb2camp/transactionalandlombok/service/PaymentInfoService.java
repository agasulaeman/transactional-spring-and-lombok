package com.gangzarb2camp.transactionalandlombok.service;

import com.gangzarb2camp.transactionalandlombok.entity.Customer;
import com.gangzarb2camp.transactionalandlombok.entity.PaymentInfo;
import com.gangzarb2camp.transactionalandlombok.repository.PaymentInfoRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PaymentInfoService {

    private PaymentInfoRepository paymentInfoRepository;

    public PaymentInfoService(PaymentInfoRepository paymentInfoRepository) {
        this.paymentInfoRepository = paymentInfoRepository;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public PaymentInfo save(Customer customer, int nominal) {
        PaymentInfo paymentInfoBuild = PaymentInfo.builder().nominal(nominal).customer(customer)
                .build();
        PaymentInfo paymentInfoSave = paymentInfoRepository.save(paymentInfoBuild);
        return paymentInfoSave;
    }
}
